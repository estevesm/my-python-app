#the configuration of the client is found in keycloak.json

#flask module for the http requests
from flask import Flask, redirect, request, jsonify, session, Response
#keycloak module for connection with the authentication server
import keycloak
import os
import uuid

#setting the flask instance
api = Flask(__name__)
#secret key is used for securely signing the session cookie and can be used for any other security related needs by extensions or your application
#should be changed in your application
#It should be a long random string of bytes
# api.config['SECRET_KEY'] = 'EYxuFcNqGamVU78GgfupoO5N4z2xokA58XtL0ag'
api.config['SECRET_KEY'] = os.environ['SUGGESTED_COOKIE_SECRET']

#creating a keycloak instance that will get the configurations from the "keycloak.json" file
# please note, redirect_uri should be passed as input parameter to Client class
# and can't be supplied via keycloak.jon configuration file
keycloak_client = keycloak.KeycloakOpenID(realm_name="cern",
                    server_url="https://auth.cern.ch/auth/",
                    client_id=os.environ['CLIENT_ID'],
                    client_secret_key=os.environ['CLIENT_SECRET'])

#KeycloakOpenID(callback_uri='http://localhost:5000/login/callback')

#the logging in happens at this stage
@api.route('/login', methods=['GET'])
def login():
    """ Initiate authentication """
    #creating an authentication call
    #auth_url is the url containing the authentication endpoint with the configurations
    #state contains a random UUID
    auth_url = keycloak_client.auth_url(redirect_uri=os.environ['REDIRECT_URI'])
    state = uuid.uuid1()
    #initializing the state session
    session['state'] = state
    #sending the request
    return redirect(auth_url)

#the redirect uri is handeled here
@api.route('/login/callback', methods=['GET'])
def login_callback():
    """ Authentication callback handler """
    # get the state value from our request and validate it in current session
    state = request.args.get('state', 'unknown')
    _state = session.pop('state', None)
    if state != _state:
        return Response('Invalid state', status=403)

    # fetch code value from our request to auth server
    code = request.args.get('code')
    # make a callback call to get token for our code value
    # the reponse value is a JSON dict with different attributes such as
    # access_token, refresh_token, etc.
    response = keycloak_client.token(code=code)

    # retrieve access token from our response
    access_token = response["access_token"]

    # using access token make request to auth server to fetch user parameters
    userinfo = keycloak_client.userinfo(access_token)

    # put user information dict returned by OAuth server into our session structure
    session["user"] = userinfo

    # return userinfo to upstream caller
    return jsonify(userinfo)

#By default the server is running at port 5000
if __name__ == '__main__':
    api.run(host='0.0.0.0', port=8080)
